# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import pyautogui
import random
from datetime import datetime, timedelta
import time
import imgui
import imgui.integrations.glfw
import glfw
import OpenGL.GL as gl

#
#
class Range:
    Min=0
    Max=0
    def __init__(self, min, max):
        self.Min = min
        self.Max = max

#
#
class Area:
    RangeX = None
    RangeY = None
    def __init__(self, rangeX, rangeY):
        self.RangeX = rangeX
        self.RangeY = rangeY


class TimeRecord:
    Time: datetime
    Diff: float
    def __init__(self, time: datetime, diff: float):
        self.Time = time
        self.Diff = diff


#
#
future = datetime.now() + timedelta(seconds=5)
labelState = "Start"
dx, dy = pyautogui.size()
defaultArea = Area(Range(0,dx), Range(0, dy))
timeStart = datetime.now()
prevTimes = []
appBeenStopped = False


def MouseMove(area):
    global future
    present = datetime.now()

    if present >= future:
        #pyautogui.leftClick()
        x = random.randint(area.RangeX.Min, area.RangeX.Max)
        y = random.randint(area.RangeY.Min, area.RangeY.Max)
        speed = random.uniform(0.25, 0.75)
        pyautogui.moveTo(x, y, speed)
        future = datetime.now() + timedelta(seconds=5)


def PressKeys():
    global future
    present = datetime.now()

    if present >= future:
        rv = random.randint(0, 100)
        if rv < 50:
            rvc = random.randint(1, 7)
            for k in range(rvc):
                pyautogui.press("up")
        else:
            rvc = random.randint(1, 7)
            for k in range(rvc):
                pyautogui.press("down")
        future = datetime.now() + timedelta(seconds=5)


def CreateWindow(w, h, title):
    if not glfw.init():
        print("Can;t init glfw!")
        exit(1)

    glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 3);
    glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, 3);
    glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE);
    glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE);
    glfw.window_hint(glfw.OPENGL_FORWARD_COMPAT, gl.GL_TRUE)

    window = glfw.create_window(w, h, title, None, None)
    if not window:
        glfw.terminate()
        print("Can;t create glfw window!")
        exit(1)

    glfw.make_context_current(window)

    return window


def MainLoop(window):
    imgui.create_context()
    imgui.get_io().display_size = 100, 100
    imgui.get_io().fonts.get_tex_data_as_rgba32()

    backend = imgui.integrations.glfw.GlfwRenderer(window)

    while not glfw.window_should_close(window):
        glfw.poll_events()
        backend.process_inputs()
        imgui.new_frame()

        UiRender()

        global labelState
        global defaultArea
        if labelState == "End":
            # MouseMove(defaultArea)
            PressKeys()

        gl.glClearColor(0.4,0.4,0.4,1.0)
        gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)

        imgui.render()
        imgui.end_frame()
        backend.render(imgui.get_draw_data())
        glfw.swap_buffers(window)

    backend.shutdown()
    glfw.terminate()


def UiRender():
    global labelState
    global timeStart
    global appBeenStopped
    global prevTimes

    imgui.begin("Your first window!", True)
    imgui.text("Hello world!")

    if imgui.button(labelState):
        if labelState == "Start":
            labelState = "End"
        else:
            labelState = "Start"
            appBeenStopped = True

    # App Started
    diffTime = datetime.now() - timeStart
    if labelState == "End":
        imgui.text(str(diffTime))
    # App Stopped
    elif labelState == "Start" and appBeenStopped:
        timeStart = datetime.now()
        prevTimes.append(TimeRecord(timeStart, diffTime))
        appBeenStopped = False


    prevTimesLen = len(prevTimes)
    for i in range(prevTimesLen):
        prevTime: TimeRecord = prevTimes[prevTimesLen - i - 1]
        imgui.text("[" + str(i) + "]" + "Time: " + prevTime.Time.strftime("%H:%M:%S") + ": [" + str(prevTime.Diff) + "]")

    imgui.end()


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    window = CreateWindow(400, 500, "Window")
    MainLoop(window)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
